﻿public static class EnumExtension
{
    public static IEnumerable<T> Interate<T>(this T c, Func<T, IEnumerable<T>> childFn)
    {
        Queue<T> queue = new Queue<T>();
        queue.Enqueue(c);
        var root = c;
        while (queue.Any())
        {
            var p = queue.Dequeue();
            // yield return p;
            foreach (var a in childFn(p))
            {
                queue.Enqueue(a);
            }
        }

        yield return root;
    }
}