﻿using Newtonsoft.Json;


public static class Util
{
    private static string[] _lines;
    private static int Count = 0;

    public static string FileName
    {
        set => _lines = File.ReadAllLines(Path.Combine(@"C:\dotnet-proj\solver\tests\", value));
    }

    public static T[] GetArray<T>()
    {
        return JsonConvert.DeserializeObject<List<T>>(_lines[Count++])!.ToArray();
    }

    public static int[][] Get2DArray()
    {
        return JsonConvert.DeserializeObject<List<List<int>>>(_lines[Count++])!
            .Select(o => o.ToArray()).ToArray();
    }

    public static T GetTreeNodes<T>() where T : TreeNode
    {
        var lst = GetArray<int?>();
        List<T> nodeLst = Enumerable.Range(0, lst.Length).Select(o =>
        {
            var d = (T)Activator.CreateInstance<T>();
            if (lst[o].HasValue)
                d.val = lst[o].Value;
           
            return d;
        }).ToList();

        for (int i = 0; i < lst.Length; i++)
        {
            if (2 * i + 1 >= 0 && 2 * i + 1 < nodeLst.Count)
                nodeLst[i].left = nodeLst[2 * i + 1];
            if (2 * i + 2 >= 0 && 2 * i + 2 < nodeLst.Count)
                nodeLst[i].right = nodeLst[2 * i + 2];
        }

        return nodeLst[0];

        // [989,null,10250,98693,-89388,null,null,null,-32127]
    }

    public static T? Get<T>()
    {
        if (typeof(T) == typeof(string)) return (T)(object)_lines[Count++];
        return JsonConvert.DeserializeObject<T>(_lines[Count++]);
    }

    public static string Dump(object s)
    {
        return JsonConvert.SerializeObject(s);
    }
}