﻿public class UnionFind
{
    private readonly int[] _parent;
    private int[] _mergeCount;

    public UnionFind(int size)
    {
        _mergeCount = new int[size];

        _parent = new int[size];
        for (int i = 0; i < _parent.Length; i++)
        {
            _parent[i] = i;
        }
    }

    public int GetParent(int u)
    {
        var pr = _parent[u];
        if (pr == u) return u;
        return _parent[u] = GetParent(pr);
    }

    

    public void Union(int x, int y)
    {
        var parentX = GetParent(x);
        var parentY = GetParent(y);
        if (_mergeCount[parentX] > _mergeCount[parentY])
        {
            (parentX, parentY) = (parentY, parentX);
        }

        _mergeCount[parentX] += _mergeCount[parentY] + 1;
        _parent[parentX] = _parent[parentY];
    }
}