﻿using System.Diagnostics.CodeAnalysis;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution
{
    private readonly Dictionary<int, int> _sum = new();

    void GetSum(TreeNode node, int level)
    {
        if (node == null) return;
        if (!_sum.ContainsKey(level))
            _sum.Add(level, node.val);
        else
            _sum[level] += node.val;
        GetSum(node.left, level + 1);
        GetSum(node.right, level + 1);
    }

    public int MaxLevelSum(TreeNode root)
    {
        var k = new List<int>();
        
        GetSum(root, 1);
        var ret = _sum.MaxBy(o => o.Value).Key;
        return ret;
    }
}